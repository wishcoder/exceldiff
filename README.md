# WishCoder - ExcelDiff



# README #

### What is this repository for? ###

* This Application compares Data in two Excel Files.

* Version 1.0.0

### How do I get set up? ###
 
* View [ExcelDiff](https://bitbucket.org/wishcoder/exceldiff/wiki/Home) wiki for more details

### Legal information ###

The library is licensed under the  LGPLv3.0 - see [LICENSE.txt](https://bitbucket.org/wishcoder/jcs.marketdata.replay/raw/a3393ba7c720cdbb17411055dc54c2aff94dbbb7/jcs.marketdata.replay/LICENSE.txt)

### Who do I talk to? ###
* Ajay Singh [message4ajay@gmail.com]
